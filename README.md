Todo:
push master to bitbucket and to heroku and verify

What I need?
1. A way to record all my coding thoughts and todos and organize them effectively.
2. How to change shell prompt so that I can see the name of the branch? using git-bash prompt


Git
git checkout -b new_branch_name
# make changes and test
git add -A
git commit -m "Changes made"
git checkout master
git merge new_branch_name
git push


Heroku Notes

heroku create
git push heroku master
heroku list
heroku apps:destroy --app app_name










Rails Notes

Undoing rails generate
$ rails generate controller StaticPages home help
$ rails destroy  controller StaticPages home help

$ rails generate model User name:string email:string
$ rails destroy model User



$ bundle exec rake db:migrate
Undoing db:migrate
$ bundle exec rake db:rollback

Clear all data in database, be very careful
$ rake db:reset

Experimenting with rails that have no lasting effect to database
rails console --sanbox

Running Rails auto tests
bundle exec rake test

Creating integration tests
rails generate integration_test site_layout

Running integration tests only
bundle exec rake test:integration

Running only tests for models
bundle exect rake test:models


I will try out the following work-flow.

My work flow

New Project
Create new repo on bitbucket (private/public)
Git init and make connection from laptop to git

New Major Feature
Create new branch
Follow instructions set in New Feature/Sub-feature but making sure to push to branch and not to master
  git push -u origin branch_name
Deploy on Heroku
Merge into Master

New Feature/Sub-feature
Create new spec for feature, test so it fails
Create new feature, test so it passes
Test on localhost
Git add/commit (push as needed)



# Ruby on Rails Tutorial: sample application

This is the sample application for the
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).
Rails shortcuts

Full command	Shortcut
$ rails server	$ rails s
$ rails console	$ rails c
$ rails generate	$ rails g
$ bundle install	$ bundle
$ rake test	$ rake